source 'https://rubygems.org'

ruby '2.1.0'

gem 'rails', '4.0.4'

# data stores
gem 'mysql2'
gem 'redis-rails'
gem 'sqlite3'

# authentication/authorization
gem 'devise'
gem 'pundit'

# HTML
gem 'slim'
gem 'simple_form'

# (S)CSS
gem 'sass-rails'
gem 'bootstrap-sass'
gem 'font-awesome-rails'
gem 'compass-rails' # for image sprites

# JavaScript/CoffeeScript
gem 'coffee-rails'
gem 'coffee-rails-source-maps'
gem 'skim'
gem 'uglifier'
gem 'therubyracer', platforms: :ruby
gem 'jquery-rails'

# all assets
gem 'vendorer'
# TODO: gem 'asset_sync'

# data model additions
gem 'enumerize'
gem 'friendly_id'
gem 'geocoder'

# API
gem 'active_model_serializers'
gem 'gon'
gem 'js-routes'

# development tools
gem 'foreman', group: :development
gem 'better_errors', group: :development
gem 'binding_of_caller', group: :development
gem 'jazz_hands', group: [:development, :test]

# code coverage and analysis
gem 'rails-erd', group: :development
gem 'annotate', group: :development
gem 'bullet', group: :development # TODO
gem 'simplecov', :require => false, group: [:development, :test]
gem 'rubocop', group: :development

# testing
gem 'database_cleaner', group: :test
gem 'factory_girl_rails', group: [:development, :test]
gem 'ffaker', group: [:development, :test]
gem 'rspec-rails', group: [:development, :test]
gem 'jasmine-rails', group: [:development, :test]
gem 'capybara', group: :test
gem 'selenium-webdriver', group: :test
