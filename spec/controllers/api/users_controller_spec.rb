require 'spec_helper'

module Api
  describe UsersController do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in :user, @user
    end

    describe "GET me" do
      it "sets current_user" do
        other_user = FactoryGirl.create(:user)
        get :me
        expect(assigns(:user)).to eq(@user)
      end
    end
  end
end
