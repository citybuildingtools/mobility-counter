require 'spec_helper'

module Api
  describe ProjectsController do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in :user, @user
    end
    
    describe "GET index" do
      it 'includes only projects authorized to the current user' do
        project1 = FactoryGirl.create(:project)
        project2 = FactoryGirl.create(:project)
        project2.users << @user
        get :index
        expect(assigns(:projects)).to match_array([project2])
      end
    end

    describe "GET show" do
      before(:each) do
        @project = FactoryGirl.create(:project)
      end

      it "sets project" do
        @project.users << @user
        get :show, id: @project.id
        expect(assigns(:project)).to eq(@project)
      end

      it "throws an authorization error if user is not a project member" do
        get :show, id: @project.id  
        expect(response.status).to eq(401)
      end
    end
  end
end
