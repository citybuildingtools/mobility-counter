require 'spec_helper'

module Api
  describe VisitorsController do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in :user, @user

      @project = FactoryGirl.create(:project)
    end
    
    describe "GET index" do
      it "selects @visitors" do
        location1 = FactoryGirl.create(:location, project: @project)
        other_visitor = FactoryGirl.create(:visitor, location: location1)
        location2 = FactoryGirl.create(:location, project: @project)
        visitors = FactoryGirl.create_list(:visitor, 2, location: location2)

        get :index, project_id: @project.id, location_id: location2.id
        expect(assigns(:visitors)).to eq(visitors)
      end

    end

    context "GET show" do
      before(:each) do
        @location1 = FactoryGirl.create(:location, project: @project)
        @visitor1 = FactoryGirl.create(:visitor, location: @location1)
        @location2 = FactoryGirl.create(:location, project: @project)
        @visitor2 = FactoryGirl.create(:visitor, location: @location2)
      end

      it "assigns @visitor" do
        get :show, project_id: @project.id, location_id: @location1.id, id: @visitor1.id
        expect(assigns(:visitor)).to eq(@visitor1)
      end
    end

    context "POST create" do
      before(:each) do
        @location = FactoryGirl.create(:location, project: @project)
      end

      it "can create a visitor" do
        post :create, project_id: @project.id, location_id: @location.id, visitor: {
          zip_code: '94024'
        }
        expect(response.status).to eq(201)
        expect(@location.visitors.count).to eq(1)
      end

      it "won't create an improper visitor" do
        post :create,
             project_id: @project.id,
             location_id: @location.id, 
             visitor: { travel_mode: 'random' }
        expect(response.status).to eq(422)
        expect(@location.visitors.count).to eq(0)
      end
    end

    context "PUT update" do
      before(:each) do
        @location = FactoryGirl.create(:location, project: @project)
        @visitor = FactoryGirl.create(:visitor, location: @location)
      end

      it "can update a visitor" do
        put :update,
            project_id: @project.id,
            location_id: @location.id, 
            id: @visitor.id, 
            visitor: { zip_code: '94024' }
        expect(response.status).to eq(200)
        expect(@visitor.reload.zip_code).to eq('94024')
      end

      it "won't process an invalid update" do
        put :update,
            project_id: @project.id,
            location_id: @location.id,
            id: @visitor.id,
            visitor: { travel_mode: 'random' }
        expect(response.status).to eq(422)
      end
    end

    context "DELETE destroy" do
      before(:each) do
        @location = FactoryGirl.create(:location, project: @project)
        @visitor = FactoryGirl.create(:visitor, location: @location)
      end

      it "can delete a visitor" do
        delete :destroy, project_id: @project.id, location_id: @location.id, id: @visitor.id
        expect(response.status).to eq(200)
        expect(Visitor.count).to eq(0)
        expect(Location.count).to eq(1)
      end

      it "will give an error on failure" do
        Visitor.any_instance.stub(:destroy) { false }
        delete :destroy, project_id: @project.id, location_id: @location.id, id: @visitor.id
        expect(response.status).to eq(500)
        expect(Visitor.count).to eq(1)
      end
    end
  end
end
