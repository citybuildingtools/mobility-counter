require 'spec_helper'

module Api
  describe LocationsController do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in :user, @user

      @project = FactoryGirl.create(:project)
    end

    context "GET index" do
      it "assigns @locations" do
        location1 = FactoryGirl.create(:location, project: @project)
        location2 = FactoryGirl.create(:location, project: @project)
        get :index, project_id: @project.id
        expect(assigns(:locations)).to eq([location1, location2])
      end
    end

    context "GET show" do
      before(:each) do
        @location1 = FactoryGirl.create(:location, project: @project)
        @location2 = FactoryGirl.create(:location, project: @project)
      end

      it "assigns @location" do
        get :show, project_id: @project.id, id: @location2.id
        expect(assigns(:location)).to eq(@location2)
      end

      it "returns a 404 if not found" do
        get :show, project_id: @project.id, id: 5
        expect(response.status).to eq(404)
      end
    end

    context "POST create" do
      it "can create a location" do
        post :create, project_id: @project.id, location: {
          name: Faker::Lorem.word,
          longitude: rand(-180.000000000...180.000000000),
          latitude: rand(-90.000000000...90.000000000)
        }
        expect(response.status).to eq(201)
        expect(Location.count).to eq(1)
      end

      it "won't create an improper location" do
        post :create, project_id: @project.id, location: {
          longitude: rand(-180.000000000...180.000000000),
          latitude: rand(-90.000000000...90.000000000)
        }
        expect(response.status).to eq(422)
        expect(Location.count).to eq(0)
      end
    end

    context "PUT update" do
      before(:each) do
        @location = FactoryGirl.create(:location, project: @project)
      end

      it "can update a location" do
        put :update, project_id: @project.id, id: @location.id, location: { name: 'New Name' }
        expect(response.status).to eq(200)
        expect(@location.reload.name).to eq('New Name')
      end

      it "won't process an invalid update" do
        put :update, project_id: @project.id, id: @location.id, location: { longitude: nil }
        expect(response.status).to eq(422)
      end
    end

    context "DELETE destroy" do
      before(:each) do
        @location = FactoryGirl.create(:location, project: @project)
      end

      it "can delete a location" do
        delete :destroy, project_id: @project.id, id: @location.id
        expect(response.status).to eq(200)
        expect(Location.count).to eq(0)
      end

      it "will give an error on failure" do
        Location.any_instance.stub(:destroy) { false }
        delete :destroy, project_id: @project.id, id: @location.id
        expect(response.status).to eq(500)
        expect(Location.count).to eq(1)
      end
    end
  end
end
