# == Schema Information
#
# Table name: projects
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  count_targets       :string(255)
#  interview_targets   :string(255)
#  southwest_latitude  :decimal(15, 10)
#  southwest_longitude :decimal(15, 10)
#  northeast_latitude  :decimal(15, 10)
#  northeast_longitude :decimal(15, 10)
#  created_at          :datetime
#  updated_at          :datetime
#

FactoryGirl.define do
  factory :project do
    name { Faker::Lorem.word }
    count_targets { 
      Project.count_targets.values.sample(
        rand(0..Project.count_targets.values.size)
      )
    }
    interview_targets {
      Project.interview_targets.values.sample(
        rand(0..Project.interview_targets.values.size)
      )
    }
    southwest_longitude { rand(-180.000000000...180.000000000) }
    southwest_latitude { rand(-90.000000000...90.000000000) }
    northeast_longitude { rand(-180.000000000...180.000000000) }
    northeast_latitude { rand(-90.000000000...90.000000000) }
  end
end
