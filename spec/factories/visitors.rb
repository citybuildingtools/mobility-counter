# == Schema Information
#
# Table name: visitors
#
#  id                :integer          not null, primary key
#  location_id       :integer
#  latitude          :decimal(15, 10)
#  longitude         :decimal(15, 10)
#  point_type        :string(255)
#  zip_code          :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  travel_mode       :string(255)
#  distance_traveled :decimal(10, 2)
#
# Indexes
#
#  index_visitors_on_location_id  (location_id)
#

FactoryGirl.define do
  factory :visitor do
    location
    travel_mode { Visitor.travel_mode.values.sample.to_sym }

    factory :visitor_from_point_on_map do
      point_type :point_on_map
      longitude { rand(-180.000000000...180.000000000) }
      latitude { rand(-90.000000000...90.000000000) }
    end

    factory :visitor_from_zip_code do
      zip_code { Faker::AddressUS.zip_code }
    end
  end
end
