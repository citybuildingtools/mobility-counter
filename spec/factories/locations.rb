# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  latitude       :decimal(15, 10)
#  longitude      :decimal(15, 10)
#  prompt_type    :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  visitors_count :integer
#  project_id     :integer
#
# Indexes
#
#  index_locations_on_project_id  (project_id)
#

FactoryGirl.define do
  factory :location do
    project
    name { Faker::Lorem.word }
    longitude { rand(-180.000000000...180.000000000) }
    latitude { rand(-90.000000000...90.000000000) }
  end
end
