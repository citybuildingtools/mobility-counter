# == Schema Information
#
# Table name: projects
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  count_targets       :string(255)
#  interview_targets   :string(255)
#  southwest_latitude  :decimal(15, 10)
#  southwest_longitude :decimal(15, 10)
#  northeast_latitude  :decimal(15, 10)
#  northeast_longitude :decimal(15, 10)
#  created_at          :datetime
#  updated_at          :datetime
#

require 'spec_helper'

describe Project do
  before(:each) do
    @project = FactoryGirl.create(:project)
  end

  it 'can be created' do
    Project.exists?(@project.id).should be_true
  end

  it 'with_member' do
    user1 = FactoryGirl.create(:user)
    user2 = FactoryGirl.create(:user)
    @project.users << user2
    expect(Project.with_member(user2)).to match_array([@project])
    expect(Project.with_member(user1)).to match_array([])
  end
end
