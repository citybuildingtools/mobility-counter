# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  latitude       :decimal(15, 10)
#  longitude      :decimal(15, 10)
#  prompt_type    :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  visitors_count :integer
#  project_id     :integer
#
# Indexes
#
#  index_locations_on_project_id  (project_id)
#

require 'spec_helper'

describe Location do
  before(:each) do
    @location = FactoryGirl.create(:location)
  end

  it 'can be created' do
    @location.persisted?.should == true
  end
end
