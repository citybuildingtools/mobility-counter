# == Schema Information
#
# Table name: project_members
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  project_id :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_project_members_on_project_id  (project_id)
#  index_project_members_on_user_id     (user_id)
#

require 'spec_helper'

describe ProjectMember do
  it 'can be created' do
    project_member = FactoryGirl.create(:project_member)
    ProjectMember.exists?(project_member.id).should be_true
  end
end
