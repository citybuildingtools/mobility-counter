# == Schema Information
#
# Table name: visitors
#
#  id                :integer          not null, primary key
#  location_id       :integer
#  latitude          :decimal(15, 10)
#  longitude         :decimal(15, 10)
#  point_type        :string(255)
#  zip_code          :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  travel_mode       :string(255)
#  distance_traveled :decimal(10, 2)
#
# Indexes
#
#  index_visitors_on_location_id  (location_id)
#

require 'spec_helper'

describe Visitor do
  context 'from a ZIP code' do
    it 'can be created' do
      visitor = FactoryGirl.create(:visitor_from_zip_code)
      visitor.persisted?.should == true
    end

    it 'will be georeferenced based on ZCTA' do
      visitor = FactoryGirl.create(:visitor_from_zip_code, zip_code: 94024)
      visitor.latitude.should == 37.352234
      visitor.longitude.should == -122.094399
      visitor.distance_traveled.should > 0
      visitor.point_type.should == 'zcta_centroid'
    end
  end

  context 'from a point on map' do
    visitor = FactoryGirl.create(:visitor_from_point_on_map)
    visitor.distance_traveled.should > 0
    visitor.point_type.should == 'point_on_map'
  end
end
