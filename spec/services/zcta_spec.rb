require 'spec_helper'

describe Zcta do
  it 'returns an ZCTA object with a latitude and a longitude' do
    zcta = Zcta.zip_to_centroid(94024)
    zcta.zip_code.should == 94024
    zcta.latitude.should == 37.352234
    zcta.longitude.should == -122.094399
  end

  it 'should return nil when no ZCTA is found' do
    zcta = Zcta.zip_to_centroid(00000)
    zcta.should be_nil
  end
end