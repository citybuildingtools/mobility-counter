describe ProjectPolicy do
  subject { ProjectPolicy }

  permissions :create? do
    it 'allowed for all' do
      user = FactoryGirl.build(:user)
      project = FactoryGirl.build(:project)
      expect(subject).to permit(user, project)
    end
  end

  permissions :show? do
    it 'not allowed for non-members' do
      user = FactoryGirl.build(:user)
      project = FactoryGirl.build(:project)
      expect(subject).not_to permit(user, project)
    end

    it 'allowed for members' do
      user = FactoryGirl.build(:user)
      project = FactoryGirl.build(:project)
      project.users << user
      expect(subject).to permit(user, project)
    end
  end

  permissions :update? do
    it 'not allowed for non-members' do
      user = FactoryGirl.build(:user)
      project = FactoryGirl.build(:project)
      expect(subject).not_to permit(user, project)
    end

    it 'allowed for members' do
      user = FactoryGirl.build(:user)
      project = FactoryGirl.build(:project)
      project.users << user
      expect(subject).to permit(user, project)
    end
  end

  permissions :destroy? do
    it 'not allowed for non-members' do
      user = FactoryGirl.build(:user)
      project = FactoryGirl.build(:project)
      expect(subject).not_to permit(user, project)
    end

    it 'allowed for members' do
      user = FactoryGirl.build(:user)
      project = FactoryGirl.build(:project)
      project.users << user
      expect(subject).to permit(user, project)
    end
  end
end
