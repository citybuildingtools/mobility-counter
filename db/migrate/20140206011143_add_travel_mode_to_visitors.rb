class AddTravelModeToVisitors < ActiveRecord::Migration
  def change
    add_column :visitors, :travel_mode, :string
  end
end
