class CreateVisitors < ActiveRecord::Migration
  def change
    create_table :visitors do |t|
      t.references :location, index: true
      t.decimal :latitude, precision: 15, scale: 10
      t.decimal :longitude, precision: 15, scale: 10
      t.string :point_type
      t.string :street_address
      t.string :zip_code
      t.string :city
      t.string :state
      t.string :country
      t.timestamps
    end
  end
end
