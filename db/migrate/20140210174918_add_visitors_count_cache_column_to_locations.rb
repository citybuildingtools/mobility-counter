class AddVisitorsCountCacheColumnToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :visitors_count, :integer
  end
end
