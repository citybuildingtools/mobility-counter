class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :count_targets
      t.string :interview_targets
      t.decimal :southwest_latitude,  precision: 15, scale: 10
      t.decimal :southwest_longitude, precision: 15, scale: 10
      t.decimal :northeast_latitude,  precision: 15, scale: 10
      t.decimal :northeast_longitude, precision: 15, scale: 10
      t.timestamps
    end
  end
end
