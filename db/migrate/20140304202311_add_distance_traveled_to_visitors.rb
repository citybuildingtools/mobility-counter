class AddDistanceTraveledToVisitors < ActiveRecord::Migration
  def change
    add_column :visitors, :distance_traveled, :decimal, precision: 10, scale: 2
  end
end
