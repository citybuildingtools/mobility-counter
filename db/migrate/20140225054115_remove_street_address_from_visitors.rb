class RemoveStreetAddressFromVisitors < ActiveRecord::Migration
  def change
    remove_column :visitors, :street_address
  end
end
