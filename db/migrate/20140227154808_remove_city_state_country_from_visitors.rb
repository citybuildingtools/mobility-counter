class RemoveCityStateCountryFromVisitors < ActiveRecord::Migration
  def change
    remove_column :visitors, :city
    remove_column :visitors, :state
    remove_column :visitors, :country
  end
end
