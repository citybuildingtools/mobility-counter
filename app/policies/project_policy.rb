ProjectPolicy = Struct.new(:current_user, :project) do
  def show?
    project.users.include? current_user
  end

  def create?
    true
  end

  def update?
    project.users.include? current_user
  end

  def destroy?
    project.users.include? current_user
  end

  self::Scope = Struct.new(:current_user, :scope) do
    def resolve
      scope.with_member(current_user)
    end
  end
end
