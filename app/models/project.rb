# == Schema Information
#
# Table name: projects
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  count_targets       :string(255)
#  interview_targets   :string(255)
#  southwest_latitude  :decimal(15, 10)
#  southwest_longitude :decimal(15, 10)
#  northeast_latitude  :decimal(15, 10)
#  northeast_longitude :decimal(15, 10)
#  created_at          :datetime
#  updated_at          :datetime
#

class Project < ActiveRecord::Base
  has_many :project_members, dependent: :destroy
  has_many :users, through: :project_members

  has_many :locations, dependent: :destroy

  scope :with_member, -> (member) do
    joins(:project_members).where(
      project_members: {
        user_id: member.id
      }
    )
  end
  
  validates :name, presence: true

  extend Enumerize
  enumerize :count_targets, in: [
    :pedestrians,
    :cyclists,
    :parked_cars
  ], multiple: true
  enumerize :interview_targets, in: [
    :pedestrians
  ], multiple: true
end
