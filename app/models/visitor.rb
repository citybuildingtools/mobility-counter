# == Schema Information
#
# Table name: visitors
#
#  id                :integer          not null, primary key
#  location_id       :integer
#  latitude          :decimal(15, 10)
#  longitude         :decimal(15, 10)
#  point_type        :string(255)
#  zip_code          :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  travel_mode       :string(255)
#  distance_traveled :decimal(10, 2)
#
# Indexes
#
#  index_visitors_on_location_id  (location_id)
#

class Visitor < ActiveRecord::Base
  belongs_to :location, counter_cache: true

  validates :location, presence: true

  extend Enumerize
  enumerize :point_type, in: [
    :point_on_map,
    :zcta_centroid
  ]
  enumerize :travel_mode, in: [
    :foot,
    :bicycle,
    :transit,
    :car
  ], scope: true

  before_save :geolocate, :compute_distance_traveled

  def geolocate
    if zip_code.present? && point_type.blank?
      centroid = Zcta.zip_to_centroid(zip_code)
      if centroid.present?
        self.latitude = centroid.latitude
        self.longitude = centroid.longitude
        self.point_type = :zcta_centroid
      else
        true
      end
    else
      true
    end
  end

  def compute_distance_traveled
    if latitude.present? && longitude.present?
      miles = Geocoder::Calculations.distance_between([latitude, longitude], [location.latitude, location.longitude])
      self.distance_traveled = miles
    else
      true
    end
  end
end
