# == Schema Information
#
# Table name: locations
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  latitude       :decimal(15, 10)
#  longitude      :decimal(15, 10)
#  prompt_type    :string(255)
#  created_at     :datetime
#  updated_at     :datetime
#  visitors_count :integer
#  project_id     :integer
#
# Indexes
#
#  index_locations_on_project_id  (project_id)
#

class Location < ActiveRecord::Base
  belongs_to :project
  has_many :visitors, dependent: :destroy

  validates :project, presence: true
  validates :name, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :prompt_type, presence: true

  extend Enumerize
  enumerize :prompt_type, in: [
    :point_on_map,
    :zip_code
  ], default: :point_on_map

  def mean_distance_traveled
    # TODO: cache this value
    visitors.where('distance_traveled > 0').average('distance_traveled')
  end
end
