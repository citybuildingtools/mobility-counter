require 'sqlite3'

# TODO: move this module to a separate Ruby gem
module Zcta
  Zcta = Struct.new(:zip_code, :latitude, :longitude)

  def self.zip_to_centroid(zip)
    results = db.execute "SELECT * FROM zctas WHERE geoid = #{zip}"
    if results.size > 0
      Zcta.new(results[0][0], results[0][5], results[0][6])
    else
      nil
    end
  end

  private
    def self.db
      SQLite3::Database.new(Rails.root.join("app", "services", "zcta.db").to_s)
    end
end
