class AccessController < ApplicationController
  def app
    gon.environment = Rails.env
    gon.current_user = current_user
  end
end
