class Api::VisitorsController < Api::ApiController
  before_action :set_project
  before_action :set_location
  before_action :set_visitor, only: [:show, :update, :destroy]

  def index
    @visitors = @location.visitors
    render json: @visitors
  end

  def show
    render json: @visitor
  end

  def create
    @visitor = @location.visitors.new(visitor_params)

    if @visitor.save
      render json: @visitor, status: :created
    else
      @errors = @visitor.errors
      render_error :unprocessable_entity, @visitor.errors
    end
  end

  def update
    if @visitor.update(visitor_params)
      render json: @visitor, status: :ok
    else
      @errors = @visitor.errors
      render_error :unprocessable_entity, @visitor.errors
    end
  end

  def destroy
    if @visitor.destroy
      render json: @visitor, status: :ok
    else
      render_error :internal_server_error, @visitor.errors
    end
  end

  private
    def set_project
      @project = Project.find(params[:project_id])
    end

    def set_location
      @location = @project.locations.find(params[:location_id])
    end

    def set_visitor
      @visitor = @location.visitors.find(params[:id])
    end

    def visitor_params
      params.require(:visitor).permit([
        :latitude,
        :longitude,
        :point_type,
        :zip_code,
        :created_at,
        :updated_at,
        :travel_mode
      ])
    end
end
