class Api::UsersController < Api::ApiController
  before_action :set_user, only: [:show, :update, :destroy]

  def index
    @users = User.where('')
    render json: @users
  end

  def me
    @user = current_user
    render json: @user
  end

  def show
    render json: @user
  end

  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created
    else
      render_error :unprocessable_entity, @user.errors
    end
  end

  def update
    if @user.update(user_params)
      render json: @user, status: :ok
    else
      render_error :unprocessable_entity, @user.errors
    end
  end

  def destroy
    if @user.destroy
      render json: @user, status: :ok
    else
      render_error :internal_server_error
    end
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit([
        :name,
        :email,
        :password,
        :password_confirmation
      ])
    end
end
