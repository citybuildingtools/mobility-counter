class Api::LocationsController < Api::ApiController
  before_action :set_project
  before_action :set_location, only: [:show, :update, :destroy]

  def index
    @locations = @project.locations
    render json: @locations
  end

  def show
    render json: @location
  end

  def create
    @location = @project.locations.new(location_params)

    if @location.save
      render json: @location, status: :created
    else
      render_error :unprocessable_entity, @location.errors
    end
  end

  def update
    if @location.update(location_params)
      render json: @location, status: :ok
    else
      render_error :unprocessable_entity, @location.errors
    end
  end

  def destroy
    if @location.destroy
      render json: @location, status: :ok
    else
      render_error :internal_server_error
    end
  end

  private
    def set_project
      @project = Project.find(params[:project_id])
    end

    def set_location
      @location = @project.locations.find(params[:id])
    end

    def location_params
      params.require(:location).permit([
        :name,
        :latitude,
        :longitude,
        :prompt_type
      ])
    end
end
