class Api::ProjectsController < Api::ApiController
  before_action :set_project, only: [:show, :update, :destroy]
  after_action :verify_authorized, except: :index
  after_action :verify_policy_scoped, only: [:index]

  def index
    @projects = policy_scope(Project)
    render json: @projects
  end

  def show
    render json: @project
  end

  def create
    @project = Project.new(project_params)
    authorize @project

    if @project.save
      render json: @project, status: :created
    else
      render_error :unprocessable_entity, @project.errors
    end
  end

  def update
    if @project.update(project_params)
      render json: @project, status: :ok
    else
      render_error :unprocessable_entity, @project.errors
    end
  end

  def destroy
    if @project.destroy
      render :show, status: :ok
    else
      render_error :internal_server_error
    end
  end

  private

    def set_project
      @project = Project.find(params[:id])
      authorize @project
    end

    def project_params
      params.require(:project).permit([
        :name,
        :count_targets,
        :interview_targets,
        :southwest_latitude,
        :southwest_longitude,
        :northeast_latitude,
        :northeast_longitude
      ])
    end
end
