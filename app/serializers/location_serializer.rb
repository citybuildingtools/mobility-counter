class LocationSerializer < ActiveModel::Serializer
  attributes :id, 
             :name,
             :latitude,
             :longitude,
             :prompt_type, 
             :created_at, 
             :updated_at,
             :visitor_counts,
             :mean_distance_traveled

  embed :ids
  has_many :visitors

  def visitor_counts
    {
      total: object.visitors.size,
      foot: object.visitors.with_travel_mode(:foot).size,
      bicycle: object.visitors.with_travel_mode(:bicycle).size,
      transit: object.visitors.with_travel_mode(:transit).size,
      car: object.visitors.with_travel_mode(:car).size,
      # other: object.visitors.without_travel_mode.size
    }
  end
end
