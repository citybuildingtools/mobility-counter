class VisitorSerializer < ActiveModel::Serializer
  attributes :id, 
             :latitude,
             :longitude,
             :point_type, 
             :zip_code, 
             :travel_mode,
             :distance_traveled,
             :created_at,
             :updated_at
end
