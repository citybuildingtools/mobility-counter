class ProjectSerializer < ActiveModel::Serializer
  attributes :id, 
             :name,
             :count_targets,
             :interview_targets,
             :southwest_longitude, 
             :southwest_latitude, 
             :northeast_longitude, 
             :northeast_latitude

  embed :ids
  has_many :locations
end
