@Access.module "SurveyMode", (SurveyMode, App, Backbone, Marionette, $, _) ->
  class SurveyMode.EnterPointOnMap extends App.Views.ItemView
    template: false
    id: "map"
    initialize: (options) ->
      @location = options.location
      @visitor = options.visitor
    onShow: ->
      @map = @createMap()
      @addBaseLayer()
      @addCenterCircle()
      @moveCenterCircle()
      App.reqres.setHandler "get:entered:origin", =>
        {
          "point_type": "point_on_map"
          "latitude": @map.getCenter().lat
          "longitude": @map.getCenter().lng
        }
    createMap: ->
      map = new L.Map 'map',
        center: new L.LatLng(@location.get('latitude'), @location.get('longitude'))
        zoom: 12
      map.attributionControl.setPrefix false
      map
    addBaseLayer: ->
      baseLayer = new L.tileLayer 'http://{s}.tiles.mapbox.com/v3/{key}/{z}/{x}/{y}.png',
        subdomains: 'abcd'
        key: 'drewcbt.h937gain'
        maxZoom: 18
      baseLayer.addTo @map
    addCenterCircle: ->
      @centerCircle = L.circle @map.getCenter(), 1000
      @centerCircle.addTo @map
    moveCenterCircle: ->
      @map.on "move drag", => @centerCircle.setLatLng @map.getCenter()

  class SurveyMode.EnterZipCodeForm extends App.Views.ItemView
    template: "survey_mode/enter_zip_code_form"
    initialize: ->
      App.reqres.setHandler "get:entered:origin", =>
        # validate
        {
          "zip_code": $("input[name='zip_code']").val()
        }

  class SurveyMode.AskOriginPage extends App.Views.Layout
    initialize: (options) ->
      @location = options.location
      @visitor = options.visitor
      @promptType = @location.get("prompt_type").replace(/_/g, "-")
    className: 'ask-origin-page'
    template: "survey_mode/ask_origin_page"
    regions:
      askOriginInputRegion: "#ask-origin-input"
    onShow: ->
      $("input[type=radio][value='#{@promptType}']", @el).prop("checked", true).parent().addClass("active")
      @renderEnterForm(@promptType)
    renderEnterForm: (promptTypeToRender) ->
      enterView = switch promptTypeToRender
        when "zip-code"
          new SurveyMode.EnterZipCodeForm
            location: @location
            visitor: @visitor
        when "point-on-map"
          new SurveyMode.EnterPointOnMap
            location: @location
            visitor: @visitor
      @askOriginInputRegion.show enterView
    events:
      "click a.skip-origin":   "skipOrigin"
      "click a.submit-origin": "submitOrigin"
      "change input[type=radio]": "promptTypeChanged"
    skipOrigin: ->
      @triggerMethod "complete"
    submitOrigin: ->
      attributes = App.request("get:entered:origin")
      # TODO: validate
      @visitor.set(attributes)
      @triggerMethod "complete"
    promptTypeChanged: (event) ->
      @renderEnterForm(event.currentTarget.value)
