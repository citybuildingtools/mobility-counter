@Access.module "SurveyMode", (SurveyMode, App, Backbone, Marionette, $, _) ->

  class SurveyMode.Router extends Marionette.AppRouter
    appRoutes:
      "locations/:id/survey": "survey"

  class SurveyMode.Layout extends Backbone.Marionette.Layout
    className: "survey-mode"
    template: "survey_mode/survey_mode_layout"
    regions:
      surveyPageRegion: ".survey-page.region"
    events:
      "click a.exit-survey-mode": "exitSurveyMode"
    exitSurveyMode: ->
      App.vent.trigger("stop:surveying", App.request "displayed:location")

  class SurveyMode.Controller extends App.Controllers.Base
    default: ->
      @layout = new SurveyMode.Layout
      @show @layout
    # unload: ->
    #   @layout?.close()
    #   @layout = null
    survey: (id, location) ->
      @default()
      location or= App.request "get:location", id
      App.reqres.setHandler "displayed:location", => location
      visitor = App.request "new:visitor", location.id
      askTravelModePage = new SurveyMode.AskTravelModePage
        location: location
        visitor: visitor
      @layout.surveyPageRegion.show askTravelModePage
      @listenTo visitor, "change:travel_mode", =>
        askOriginPage = new SurveyMode.AskOriginPage
          location: location
          visitor: visitor
        @layout.surveyPageRegion.show askOriginPage
        @listenTo askOriginPage, "complete", =>
          visitor.save().done =>
            # TODO: confirmation/thanks
            App.vent.trigger "start:surveying", location

  SurveyMode.addInitializer =>
    @controller = new SurveyMode.Controller
    new SurveyMode.Router
      controller: @controller

  App.vent.on "start:surveying", (location) =>
    location or= App.request "displayed:location"
    App.navigate "locations/#{location.id}/survey"
    @controller.survey(location.id, location)

  App.vent.on "stop:surveying", (location) =>
    App.vent.trigger("reload:location", location.id) # TODO: fix so that the correct count appears back in AnalyzeMode
    App.vent.trigger("show:location", location)
