@Access.module "SurveyMode", (SurveyMode, App, Backbone, Marionette, $, _) ->
  class SurveyMode.AskTravelModePage extends App.Views.ItemView
    initialize: (options) ->
      @location = options.location
      @visitor = options.visitor
    className: 'ask-travel-mode-page'
    template: "survey_mode/ask_travel_mode_page"
    events:
      "click .ask-travel-mode-button": "askTravelModeButtonClicked"
    askTravelModeButtonClicked: (event) ->
      @visitor.set
        travel_mode: $(event.target).data('travel-mode')
