@Access.module "AnalyzeMode.LocationShow", (LocationShow, App, Backbone, Marionette, $, _) ->

  class LocationShow.Controller extends App.Controllers.Base
    initialize: (options) ->
      { location, id } = options
      location or= App.request "get:location", id

      overview = new LocationShow.Overview
        model: location

      @show overview #,
        # loading:
          # entities: location

      App.reqres.setHandler "displayed:location", => location

  class LocationShow.Overview extends App.Views.ItemView
    className: 'location-show-overview'
    template: "analyze_mode/location_show_overview"
    # initialize: ->
      # @listenTo @model.visitors, "reset", @drawDistanceTraveledBarChart
    modelEvents:
      "change": "render drawTravelModePieChart"
    onShow: ->
      @model.visitors.fetch().done => @drawDistanceTraveledBarChart() # TODO!!!!!
      @drawTravelModePieChart()
    drawDistanceTraveledBarChart: ->
      $("#location-visitors-by-distance-traveled-chart", @el).empty()
      if @model.visitors?.length > 0
        values = @model.visitors.pluck("distance_traveled")
        values = _.compact values
        values = _.map values, (v) -> +v
        formatCount = d3.format(",.0f")
        margin = {top: 10, right: 30, bottom: 30, left: 30}
        width = 300 - margin.left - margin.right
        height = 250 - margin.top - margin.bottom
        x = d3.scale.linear()
            .domain([0, d3.max(values)])
            .range([0, width])
        data = d3.layout.histogram().bins(x.ticks(10))(values)
        y = d3.scale.linear()
            .domain([0, d3.max(data, (d) -> d.y )])
            .range([height, 0])
        xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
        svg = d3.select("#location-visitors-by-distance-traveled-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        bar = svg.selectAll(".bar")
            .data(data)
          .enter().append("g")
            .attr("class", "bar")
            .attr("transform", (d) -> "translate(" + x(d.x) + "," + y(d.y) + ")" )
        bar.append("rect")
            .attr("x", 1)
            .attr("width", x(data[0].dx) - 1)
            .attr("height", (d) -> height - y(d.y) )
        bar.append("text")
            .attr("dy", ".75em")
            .attr("y", 6)
            .attr("x", x(data[0].dx) / 2)
            .attr("text-anchor", "middle")
            .text((d) -> formatCount(d.y) )
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)

    drawTravelModePieChart: ->
      $("#location-visitors-by-travel-mode-chart", @el).empty()
      if @model.visitorCounts("total") > 0
        margin = {top: 10, right: 30, bottom: 30, left: 30}
        width = 300 - margin.left - margin.right
        height = 300 - margin.top - margin.bottom
        r = 100
        data = @model.visitorCountsArray()
        vis = d3.select("#location-visitors-by-travel-mode-chart")
          .append("svg:svg")
          .data([data])
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("svg:g")
            .attr("transform", "translate(" + (r + margin.left) + "," + (r + margin.top) + ")") # move the center of the pie chart from 0, 0 to radius, radius

        arc = d3.svg.arc()
          .outerRadius(r)

        pie = d3.layout.pie()
          .value (d) -> d.visitorCount

        arcs = vis.selectAll("g.slice")     # this selects all <g> elements with class slice (there aren't any yet)
          .data(pie)                          # associate the generated pie data (an array of arcs, each having startAngle, endAngle and value properties)
          .enter()                            # this will create <g> elements for every "extra" data element that should be associated with a selection. The result is creating a <g> for every object in the data array
            .append("svg:g")                # create a group to hold each slice (we will have a <path> and a <text> element associated with each slice)
            .attr("class", "slice")    # allow us to style things in the slices (like text)

        arcs.append("svg:path")
          .attr("class", (d) -> d.data.travelMode )
          .attr("d", arc)                                    # this creates the actual SVG path using the associated data (pie) with the arc drawing function

        arcLabels = vis.selectAll("g.arc-label")
          .data(pie)
          .enter()
            .append("svg:g")
            .attr("class", "arc-label")

        arcLabels.append("svg:text")                                     # add a label to each slice
          .filter (d) -> d.value > 0
          .attr "transform", (d) ->                    # set the label's origin to the center of the arc
            # we have to make sure to set these before calling arc.centroid
            d.innerRadius = r / 2
            d.outerRadius = r
            "translate(" + arc.centroid(d) + ")"        #this gives us a pair of coordinates like [50, 50]
          .attr("text-anchor", "middle")                          #center the text on it's origin
          .text (d, i) -> data[i].travelMode[0] # just the first letter of the travel mode

