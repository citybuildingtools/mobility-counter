@Access.module "AnalyzeMode.Map", (Map, App, Backbone, Marionette, $, _) ->

  class Map.Controller extends App.Controllers.Base
    initialize: ->
      @mapView = new Map.MapView
      @show @mapView

    reset: ->
      @stopListening()
      @locationMarker?.close()
      @locationMarkerLayer?.close()
      @visitorMarkerLayer?.close()

    showAllLocations: ->
      locations = App.request "get:locations"
      @locationMarkerLayer = new Map.LocationMarkerLayer
        mapView: @mapView
        collection: locations
      @locationMarkerLayer.zoomToFitAll()
      @locationMarkerLayer.render()
      @listenTo @locationMarkerLayer, "itemview:click:location:marker", (locationMarker) ->
        App.vent.trigger "show:location", locationMarker.model
      @listenTo @locationMarkerLayer, "itemview:hover:on:location:marker", (locationMarker) ->
        App.vent.trigger "hover:on:location:marker", locationMarker.model
      @listenTo @locationMarkerLayer, "itemview:hover:off:location:marker", (locationMarker) ->
        App.vent.trigger "hover:off:location:marker", locationMarker.model
      App.vent.on "hover:on:location hover:off:location", (location) =>
        @highlightLocation(location.id)

    highlightLocation: (id) ->
      $(".location.location-#{id}").toggleClass('active')

    showOneLocation: (id, location) ->
      location or= App.request "get:location", id
      @locationMarker = new Map.LocationMarker
        mapView: @mapView
        model: location
      @locationMarker.render()
      location.visitors.fetch()
      @visitorMarkerLayer = new Map.VisitorMarkerLayer
        mapView: @mapView
        collection: location.visitors
        location: location
      @visitorMarkerLayer.render()
      @visitorMarkerLayer.zoomToFitAll()

    locateMap: ->
      @mapView.locate()

    editOneLocation: (id, location) ->
      location or= App.request "get:location", id
      @locationMarker = new Map.LocationMarker
        mapView: @mapView
        model: location
      @locationMarker.zoomToThisMarker()
      @locationMarker.render()
      @listenTo @mapView, "map:move:end", =>
        center = @mapView.getCenter()
        location.set
          latitude: center.lat
          longitude: center.lng
      @mapView.trigger "map:move:end" if location?.isNew()

  class Map.LocationMarker extends App.Views.ItemView
    template: false
    initialize: (options) ->
      @mapView = options.mapView
      @listenTo @model, "change sync", @render
      @listenTo @model, "destroy", @close
    icon: ->
      L.divIcon
        className: "map marker location location-#{@model.id}"
        iconSize: [26, 26]
        html: @model.visitorCounts("total")
    render: ->
      if @model.has('latitude') and @model.has('longitude')
        @removeMarker() if @locationMarker?
        locationLatLng = new L.LatLng(@model.get('latitude'), @model.get('longitude'))
        @locationMarker = L.marker locationLatLng,
          icon: @icon()
        @locationMarker.addEventListener "mouseover", =>
          @trigger "hover:on:location:marker", @model
        @locationMarker.addEventListener "mouseout", =>
          @trigger "hover:off:location:marker", @model
        @locationMarker.addEventListener "click", =>
          @trigger "click:location:marker", @model
        @locationMarker.addTo(@mapView.getMapElement())
    zoomToThisMarker: ->
      if @model.has('latitude') and @model.has('longitude')
        @mapView.getMapElement().panTo(new L.LatLng(@model.get('latitude'), @model.get('longitude')))
        @mapView.getMapElement().setZoom(13)
    removeMarker: ->
      @mapView.getMapElement().removeLayer @locationMarker
    onClose: ->
      @removeMarker() if @locationMarker?

  class Map.LocationMarkerLayer extends App.Views.CollectionView
    itemView: Map.LocationMarker
    initialize: (options) ->
      @mapView = options.mapView
      @listenTo @collection, "add change reset destroy", @zoomToFitAll
    itemViewOptions: -> {
      mapView: @mapView
    }
    zoomToFitAll: ->
      if @collection.length > 0
        @mapView.getMapElement().fitBounds(@collection.getBounds())

  class Map.VisitorMarker extends App.Views.ItemView
    template: false
    initialize: (options) ->
      @mapView = options.mapView
      @location = options.location
      @listenTo @model, "change sync", @render
      @listenTo @model, "destroy", @close
    icon: ->
      L.divIcon
        className: "map marker visitor visitor-#{@model.id} #{@model.get("travel_mode")}"
        iconSize: [26, 26]
        html: "<i class='travelmode-icon-#{@model.get("travel_mode")}'></i>"
    travelLine: ->
      L.polyline [@model.getLatLng(), @location.getLatLng()],
        className: "visitor-travel-line #{@model.get("travel_mode")}"
    render: ->
      if @model.has('latitude') and @model.has('longitude')
        @removeFeatureGroup() if @featureGroup?
        @visitorMarker = L.marker @model.getLatLng(),
          icon: @icon()
        @featureGroup = L.featureGroup [@visitorMarker, @travelLine()]
        @featureGroup.addEventListener "mouseover", =>
          @trigger "hover:on:visitor:marker", @model
        @featureGroup.addEventListener "mouseout", =>
          @trigger "hover:off:visitor:marker", @model
        @featureGroup.addEventListener "click", =>
          @trigger "click:visitor:marker", @model
        @featureGroup.addTo(@mapView.getMapElement())
    removeFeatureGroup: ->
      @mapView.getMapElement().removeLayer @featureGroup
    onClose: ->
      @removeFeatureGroup() if @featureGroup?

  class Map.VisitorMarkerLayer extends App.Views.CollectionView
    itemView: Map.VisitorMarker
    initialize: (options) ->
      @mapView = options.mapView
      @location = options.location
    itemViewOptions: -> {
      mapView: @mapView
      location: @location
    }
    onAfterItemAdded: ->
      @zoomToFitAll()
    zoomToFitAll: ->
      if @collection.length > 0 and @collection.getBounds()? and @location?.getLatLng()?
        bounds = @collection.getBounds()
        bounds.extend(@location.getLatLng())
        @mapView.getMapElement().fitBounds(bounds)

  class Map.MapView extends Marionette.View
    id: 'map'
    initialize: ->
      App.reqres.setHandler "get:map:element", @getMapElement
    onShow: ->
      @map = @createMap()
      @addBaseLayer()
      @addLocateControl()
    createMap: ->
      map = new L.Map 'map',
        center: [37.8724988, -122.2740345]
        zoom: 13
      map.attributionControl.setPrefix false
      map.addEventListener "moveend", =>
        @trigger "map:move:end"
      map
    addBaseLayer: ->
      baseLayer = new L.tileLayer 'http://{s}.tiles.mapbox.com/v3/{key}/{z}/{x}/{y}.png',
        subdomains: 'abcd'
        key: 'drewcbt.h937gain'
        maxZoom: 18
      baseLayer.addTo @map
    addLocateControl: ->
      @locateControl = L.control.locate
        follow: false
        setView: isMobile.any()
        locateOptions:
          enableHighAccuracy: true
      @locateControl.addTo(@map)
      @locateControl.locate()
    locate: ->
      @map.locate
        setView: true
        maximumAge: 60 * 1000 # milliseconds
        enableHighAccuracy: true
    getCenter: ->
      @map.getCenter()
    getMapElement: ->
      @map
