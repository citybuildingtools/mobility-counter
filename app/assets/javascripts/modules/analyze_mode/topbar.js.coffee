@Access.module "AnalyzeMode.Topbar", (Topbar, App, Backbone, Marionette, $, _) ->

  class Topbar.Controller extends App.Controllers.Base
    initialize: ->
      view = new Topbar.View
        currentUser: App.request "get:current:user"
      @show view

  class Topbar.View extends App.Views.ItemView
    template: "analyze_mode/topbar_view"
    tagName: "nav"
    className: "navbar navbar-default"
    initialize: (options) ->
      @currentUser = options.currentUser
      @listenTo @currentUser, "all", @render
    serializeData: ->
      {
        currentUserName: @currentUser.get("name")
      }
