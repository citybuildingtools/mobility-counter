@Access.module "AnalyzeMode.LocationsList", (LocationsList, App, Backbone, Marionette, $, _) ->

  class LocationsList.Controller extends App.Controllers.Base
    initialize: ->
      locations = App.request "get:locations"

      locationsList = new LocationsList.CompositeView
        collection: locations

      @listenTo locationsList, "itemview:show:location", (locationListItemView) ->
        App.vent.trigger "show:location", locationListItemView.model

      @listenTo locationsList, "itemview:hover:on:location", (locationListItemView) ->
        App.vent.trigger "hover:on:location", locationListItemView.model

      @listenTo locationsList, "itemview:hover:off:location", (locationListItemView) ->
        App.vent.trigger "hover:off:location", locationListItemView.model

      @show locationsList,
        loading:
          entities: locations

  class LocationsList.ItemView extends App.Views.ItemView
    template: "analyze_mode/locations_list_item_view"
    tagName: "tr"
    id: -> "location-list-row-#{@model.id}"
    initialize: ->
      App.vent.on "hover:on:location:marker", (location) =>
        @toggleHighlight() if location == @model
      App.vent.on "hover:off:location:marker", (location) =>
        @toggleHighlight() if location == @model
    triggers:
      "click a.open-location":  "show:location"
      "dblclick":               "show:location"
    modelEvents:
      "updated": "render"
    events:
      "mouseenter": "mouseenter"
      "mouseleave": "mouseleave"
    mouseenter: ->
      @toggleHighlight()
      @trigger "hover:on:location", @
    mouseleave: ->
      @toggleHighlight()
      @trigger "hover:off:location", @
    toggleHighlight: ->
      $(@el).toggleClass('active')

  class LocationsList.EmptyView extends App.Views.ItemView
    template: "analyze_mode/locations_list_empty_view"

  class LocationsList.CompositeView extends Marionette.CompositeView
    className: "location-list panel panel-default"
    template: "analyze_mode/location_list_view"
    itemViewContainer: "tbody"
    itemView: LocationsList.ItemView
    emptyView: LocationsList.EmptyView
    initialize: ->
      App.vent.on "hover:on:location:marker", (location) =>
        $(@el).scrollTo("#location-list-row-#{location.id}")
