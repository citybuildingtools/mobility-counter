@Access.module "AnalyzeMode.LocationEdit", (LocationEdit, App, Backbone, Marionette, $, _) ->

  class LocationEdit.Controller extends App.Controllers.Base
    initialize: (options) ->
      { location, id } = options
      location or= App.request "get:location", id

      form = new LocationEdit.Form
        model: location

      @show form #,
        # loading:
          # entities: location

      App.reqres.setHandler "displayed:location", => location
      App.reqres.setHandler "serialize:edit:location", =>
        Backbone.Syphon.serialize(form)

  class LocationEdit.Form extends App.Views.ItemView
    className: 'location-edit-form'
    template: "analyze_mode/location_edit_form"
    modelEvents:
      "change:name change:prompt_type": "render"
