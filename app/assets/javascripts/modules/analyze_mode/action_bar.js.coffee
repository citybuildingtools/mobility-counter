@Access.module "AnalyzeMode.ActionBar", (ActionBar, App, Backbone, Marionette, $, _) ->

  class ActionBar.Controller extends App.Controllers.Base
    initialize: (options) ->
      view = new ActionBar.View
        section: options.section
      @show view

      @listenTo view, "create:location", -> App.vent.trigger "create:location"
      @listenTo view, "list:locations", -> App.vent.trigger "list:locations"
      @listenTo view, "edit:location", -> App.vent.trigger "edit:location"
      @listenTo view, "start:surveying", -> App.vent.trigger "start:surveying"
      @listenTo view, "cancel:edit:location", -> App.vent.trigger "cancel:edit:location"
      @listenTo view, "delete:edit:location", -> App.vent.trigger "delete:edit:location"
      @listenTo view, "save:edit:location", -> App.vent.trigger "save:edit:location"

  class ActionBar.View extends App.Views.ItemView
    template: "analyze_mode/action_bar_view"
    tagName: "ul"
    className: "action-bar-buttons"
    initialize: (options) ->
      @section = options.section
    serializeData: =>
      {
        section: @section
      }
    triggers:
      "click a.create-a-new-location":    "create:location"
      "click a.return-to-locations-list": "list:locations"
      "click a.edit-location":            "edit:location"
      "click a.survey-here":              "start:surveying"
      "click a.cancel-edit-location":     "cancel:edit:location"
      "click a.delete-edit-location":     "delete:edit:location"
      "click a.save-edit-location":       "save:edit:location"
