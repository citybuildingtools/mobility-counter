@Access.module "AnalyzeMode", (AnalyzeMode, App, Backbone, Marionette, $, _) ->

  class AnalyzeMode.Router extends Marionette.AppRouter
    appRoutes:
      "locations":          "list"
      "locations/:id":      "show"
      "locations/:id/edit": "newEdit"

  API =
    default: (section) ->
      @layout ?= new AnalyzeMode.Layout
      unless App.mainRegion.currentView == @layout
        App.mainRegion.show @layout
      @topbar ?= new AnalyzeMode.Topbar.Controller
        region: @layout.topbarRegion
      new AnalyzeMode.ActionBar.Controller
        region: @layout.actionBarRegion
        section: section
      @map ?= new AnalyzeMode.Map.Controller
        region: @layout.mapPanelRegion
      @map.reset()

    unload: ->
      @layout?.close()
      @topbar?.close()
      @map?.close()
      @layout = null
      @topbar = null
      @map = null

    list: ->
      @default('locations-list')
      @map.showAllLocations()
      new AnalyzeMode.LocationsList.Controller
        region: @layout.infoPanelRegion
      @layout.setHeights() # TODO

    show: (id, location) ->
      @default('location-show')
      @map.showOneLocation(id, location)
      new AnalyzeMode.LocationShow.Controller
        region: @layout.infoPanelRegion
        id: id
        location: location
      @layout.setHeights() # TODO

    newEdit: (id, location) ->
      if location?.isNew()
        @default('location-new')
        @map.locateMap()
      else
        @default('location-edit')
      @map.editOneLocation(id, location)
      new AnalyzeMode.LocationEdit.Controller
        region: @layout.infoPanelRegion
        id: id
        location: location
      @layout.setHeights() # TODO

  class AnalyzeMode.Layout extends Backbone.Marionette.Layout
    className: "analyze-mode"
    template: "analyze_mode/analyze_mode_layout"
    regions:
      topbarRegion: ".topbar.region"
      infoPanelRegion: ".info-panel.region"
      actionBarRegion: ".action-bar.region"
      mapPanelRegion: ".map-panel.region"
    onShow: =>
      @setHeights()
      # TODO: make this automatic
    setHeights: ->
      topbar = 50
      actionBar = 54
      remaining = $(window).height() - topbar - actionBar;
      $('.info-panel.region').height(remaining / 2)
      $('.map-panel.region').height(remaining / 2)

  AnalyzeMode.addInitializer ->
    new AnalyzeMode.Router
      controller: API

  App.vent.on "list:locations", ->
    App.navigate "locations"
    API.list()

  App.vent.on "show:location", (location) ->
    location or= App.request "displayed:location"
    App.navigate "locations/#{location.id}"
    API.show location.id, location

  App.vent.on "create:location", ->
    location = App.request "new:location"
    App.navigate "locations/#{location.cid}/edit"
    API.newEdit(location.id, location)

  App.vent.on "edit:location", (location) ->
    location or= App.request "displayed:location"
    App.navigate "locations/#{location.id}/edit"
    API.newEdit(location.id, location)

  App.vent.on "save:edit:location", ->
    location = App.request "displayed:location"
    params = App.request("serialize:edit:location")
    # TODO: validate
    location.set(params)
    App.request("save:location", location)
    # TODO: handle errors
    App.vent.trigger("show:location", location)

  App.vent.on "delete:edit:location", ->
    location = App.request "displayed:location"
    location.destroy() if confirm("Are you sure you want to delete this location?") # TODO: improve appearance with bootbox.js
    App.navigate "locations"
    API.list()

  App.vent.on "cancel:edit:location", ->
    location = App.request "displayed:location"
    if location.isNew()
      App.navigate "locations"
      API.list()
    else
      App.navigate "locations/#{location.id}"
      API.show location.id, location

  App.vent.on "start:surveying", =>
    API.unload()
