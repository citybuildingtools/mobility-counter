@Access.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

  class Entities.Model extends Backbone.Model
    parse: (resp, options) ->
      if resp.result?
        resp.result
      else
        resp
