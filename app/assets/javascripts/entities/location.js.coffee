@Access.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

  class Entities.Location extends App.Entities.Model
    urlRoot: -> Routes.api_locations_path()
    initialize: ->
      @visitors = new Entities.VisitorCollection
        locationId: @id
    defaults:
      "prompt_type": "point_on_map"
    mutators:
      promptTypeDisplay:
        get: ->
          promptTypes = {
            "point_on_map":   "Point on Map"
            "zip_code":       "ZIP Code"
          }
          promptTypes[@get("prompt_type")]
        transient: true
      visitor_count_foot:
        get: ->
          @visitorCounts("foot")
        transient: true
      visitor_count_bicycle:
        get: ->
          @visitorCounts("bicycle")
        transient: true
      visitor_count_transit:
        get: ->
          @visitorCounts("transit")
        transient: true
      visitor_count_car:
        get: ->
          @visitorCounts("car")
        transient: true
      visitor_count_total:
        get: ->
          @visitorCounts("total")
        transient: true
    visitorCounts: (travelMode) ->
      visitorCounts = @get("visitor_counts")
      visitorCounts or= {}
      visitorCounts = _.defaults visitorCounts, {
        "foot": 0
        "bicycle": 0
        "transit": 0
        "car": 0
        "total": 0
      }
      visitorCounts[travelMode]
    visitorCountsArray: ->
      array = []
      for key, value of @get("visitor_counts")
        unless key == "total"
          array.push({
            "travelMode": key
            "visitorCount": value
          })
      array
    getLatLng: ->
      if @has("latitude") and @has("longitude")
        new L.LatLng(@get("latitude"), @get("longitude"))

  class Entities.LocationCollection extends App.Entities.Collection
    model: Entities.Location
    url: -> Routes.api_locations_path()
    getBounds: ->
      bounds = new L.LatLngBounds()
      @each (location) ->
        bounds.extend(location.getLatLng())
      bounds

  API =
    locations: ->
      @locationCollection ?= new Entities.LocationCollection
      @locationCollection
    getLocations: (forceReload = false) ->
      if forceReload or !@lastLocationsFetch? or (@lastLocationsFetch - Date.now() > 3000 and @locations().length == 0)
        @locations().fetch({reset: true})
        @lastLocationsFetch = Date.now()
      @locations()
    getLocation: (id, forceReload = false) ->
      if id == "undefined"
        App.vent.trigger "not:found:location"
      if !forceReload && @locations().get(id)? # works for both id's and cid's
        @locations().get(id)
      else
        location = new Entities.Location
          id: id
        location.fetch()
                .fail -> App.vent.trigger "not:found:location"
        location
    newLocation: ->
      new Entities.Location
    saveLocation: (location) ->
      if location.isNew()
        @locations().create(location)
      else
        location.save()

  App.vent.on "not:found:location", ->
   App.vent.trigger "list:locations"

  App.reqres.setHandler "get:locations", ->
    API.getLocations()

  App.vent.on "reload:locations", ->
    API.getLocations(true)

  App.reqres.setHandler "get:location", (id) ->
      API.getLocation(id)

  App.vent.on "reload:location", (id) ->
    API.getLocation(id, true)

  App.reqres.setHandler "new:location", ->
    API.newLocation()

  App.reqres.setHandler "save:location", (location) ->
    API.saveLocation(location)
