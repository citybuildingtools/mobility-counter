@Access.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

  class Entities.Visitor extends App.Entities.Model
    url: ->
      Routes.api_location_visitors_path(@locationId)
    initialize: (options) ->
      @locationId = options.locationId
    getLocation: ->
      App.request "get:location", @locationId
    getLatLng: ->
      if @has("latitude") and @has("longitude")
        new L.LatLng(@get("latitude"), @get("longitude"))

  class Entities.VisitorCollection extends App.Entities.Collection
    model: Entities.Visitor
    url: ->
      Routes.api_location_visitors_path(@locationId)
    initialize: (options) ->
      @locationId = options.locationId
    getBounds: ->
      bounds = new L.LatLngBounds()
      @each (visitor) ->
        if visitor.has("latitude") and visitor.has("longitude")
          visitorLatLng = new L.LatLng(visitor.get("latitude"), visitor.get("longitude"))
          bounds.extend(visitorLatLng)
      if bounds.isValid()
        bounds
      else
        null

  API =
    newVisitor: (locationId) ->
      new Entities.Visitor
        locationId: locationId

  App.reqres.setHandler "new:visitor", (locationId) ->
    API.newVisitor(locationId)
