@Access.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->

  class Entities.User extends App.Entities.Model
    url: ->
      Routes.api_users_path()

  class Entities.VisitorCollection extends App.Entities.Collection
    model: Entities.User
    url: ->
      Routes.api_users_path()

  API =
    setCurrentUser: (attributes) ->
      @currentUser = new Entities.User(attributes)

    getCurrentUser: ->
      @currentUser

  App.reqres.setHandler "set:current:user", (attributes) ->
    API.setCurrentUser(attributes)

  App.reqres.setHandler "get:current:user", ->
    API.getCurrentUser()
