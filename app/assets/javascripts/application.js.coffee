#= require jquery
#= require underscore
#= require underscore.string
#= require backbone
#= require backbone-syphon
#= require backbone-marionette
#= require backbone-rails-sync
#= require backbone-mutators
#= require bootstrap
#= require leaflet
#= require leaflet-locatecontrol
#= require d3
#= require spin
#= require jquery.spin
#= require jquery.scrollTo
#= require is-mobile
#= require js-routes
#= require skim

#= require_self

#= require_tree ./lib/entities
#= require_tree ./lib/utilities
#= require_tree ./lib/views
#= require_tree ./lib/controllers
#= require_tree ./lib/components

#= require_tree ./entities
#= require_tree ./modules

@Access = do (Backbone, Marionette) ->
  App = new Marionette.Application

  App.rootRoute = "locations"

  App.on "initialize:before", (options) ->
    App.environment = options.environment
    if options.currentUser?
      App.request "set:current:user", options.currentUser

  App.addRegions
    mainRegion: ".main.region"

  App.reqres.setHandler "default:region", -> App.mainRegion

  App.on "initialize:after", ->
    @startHistory()
    @navigate(@rootRoute, trigger: true) unless @getCurrentRoute()

  App
