Access::Application.routes.draw do
  devise_for :users

  namespace :api do
    resources :users do
      get 'me', on: :collection
    end
    resources :projects do
      resources :locations do
        resources :visitors
      end
    end
  end

  root to: 'access#app'

  if defined?(JasmineRails) && ['development', 'test'].include?(Rails.env)
    mount JasmineRails::Engine => '/specs'
  end
end
